# Erin's Portfolio

I am remaking my Potfolio in the [Flutter Framework](https://flutter.dev) using the [Dart Programming Language](https://dart.dev). I will be using [Firebase](https://firebase.google.com/firebase) for the admin panel.

# Roadmap

## Pages to Complete
- [ ] Home (In progress)
    - [ ] Animated Text (just needs to be sped up)
    - [ ] Paragraph under Text
    - [ ] "Get In Touch" button to take you to contact page.
- [ ] Admin Section
    - [ ] Login
    - [ ] Home
    - [ ] Profile
    - [ ] Reset Password
    - [ ] Full Work Experience List
    - [ ] Add Work Experience
    - [ ] Full Project List
    - [ ] Add Project
- [ ] Public Experience List
- [ ] Public Project List
- [ ] Contact Erin
    - [ ] Make sure emails get received to [me@erinskidds.com](mailto:me@erinskidds.com)
- [ ] Link to Resume to Automatically download or view on web

## Features
- [ ] Upload to Website
- [ ] Downloadable App via Chrome
- [ ] Make sure X/Twitter & Facebook links all display fancy bookmark links.
- [ ] Do I need a logo?