import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:portfolio/components/home.dart';
import 'package:portfolio/universal/app_bar.dart';

GoRouter appRouter(BuildContext context) {
  return GoRouter(
    initialLocation: '/',
    routes: [
      GoRoute(
        name: 'home',
        path: '/',
        builder: (context, state) => const Home(title: 'Erin Skidds'),
      ),
    ],
    errorPageBuilder: (context, state) => const MaterialPage(
      child: Scaffold(
        appBar: Bar(title: 'Page Not Found'),
        body: Center(
          child: Text('Page Not Found'),
        ),
      ),
    ),
  );
}
