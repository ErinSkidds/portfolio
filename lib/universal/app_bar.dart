import 'package:universal_io/io.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import './responsive.dart';
//import 'package:flutter_pdfview/flutter_pdfview.dart';

class _FontSize extends StatelessWidget {
  const _FontSize({required this.text, required this.textColor});
  final String text;
  final Color textColor;
  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: TextStyle(
          color: textColor,
          fontSize: Responsive.isWeb(context)
              ? 20
              : Responsive.isTablet(context)
                  ? 15
                  : 10,
        ));
  }
}

class Bar extends StatelessWidget implements PreferredSizeWidget {
  const Bar({super.key, required this.title});
  final String title;

  @override
  Widget build(BuildContext context) {
    final isDarkMode = Theme.of(context).brightness == Brightness.dark;
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle(
          statusBarIconBrightness:
              isDarkMode ? Brightness.dark : Brightness.light,
          statusBarColor: Theme.of(context).primaryColorDark),
      toolbarHeight: 1000,
      iconTheme: const IconThemeData(
        color: Colors.white,
      ),
      automaticallyImplyLeading: true,
      title: Text(
        title,
        style: const TextStyle(color: Colors.white),
      ),
      backgroundColor: Theme.of(context).colorScheme.primary,
      foregroundColor: Colors.white,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class MyDrawer extends StatelessWidget {
  MyDrawer({super.key});
  final _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Drawer(
      width: 300,
      elevation: 3,
      child: Container(
        color: Theme.of(context).colorScheme.background,
        child: Scrollbar(
          thumbVisibility: true,
          trackVisibility: true,
          controller: _scrollController,
          child: SingleChildScrollView(
            controller: _scrollController,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  SizedBox(
                    height: Platform.isAndroid
                        ? 100
                        : Platform.isIOS
                            ? 130
                            : 100,
                    width: 20,
                    child: buildHeader(context),
                  ),
                  const _DrawerTile(
                    leading: Icon(Icons.home),
                    title: _Page(
                      name: 'Home',
                      url: '/',
                    ),
                  ),
                  const _DrawerTile(
                    leading: Icon(Icons.person_3_outlined),
                    title: _Page(
                      name: 'About Erin',
                      url: 'about',
                    ),
                  ),
                  const _DrawerTile(
                    leading: Icon(Icons.work),
                    title: _Page(
                      name: 'Work Experience',
                      url: 'exp',
                    ),
                  ),
                  const _DrawerTile(
                    leading: Icon(Icons.code),
                    title: _Page(
                      name: 'Projects',
                      url: 'projects',
                    ),
                  ),
                  const _DrawerTile(
                    leading: Icon(Icons.email),
                    title: _Page(
                      name: 'Contact Erin',
                      url: 'contact',
                    ),
                  ),
                  const _DrawerTile(
                    leading: Icon(Icons.email),
                    title: _Page(
                      name: 'View Erin\'s Resume',
                      url: '/',
                      // TODO: GET RESUME WORKING
                    ),
                  ),
                  AboutListTile(
                    icon: const Icon(Icons.info),
                    applicationIcon: const Icon(Icons.handyman),
                    applicationName: 'Erin Skidds',
                    applicationVersion: '1.0.0',
                    applicationLegalese: '© 2020-2024 ErinSkidds.com',
                    // aboutBoxChildren: [
                    //   Text(
                    //     'Child Text or about the app here, good for debugging stuff.',
                    //   ),
                    // ],
                    child: Text('About ErinSkidds.com',
                        style: Theme.of(context).textTheme.bodyLarge),
                  ),
                ]),
          ),
        ),
      ),
    );
  }

  DrawerHeader buildHeader(BuildContext context) {
    return DrawerHeader(
        decoration: BoxDecoration(color: Theme.of(context).colorScheme.primary),
        child: const Row(children: [
          _FontSize(
            text: 'Erin Skidds',
            textColor: Colors.white,
          ),
        ]));
  }
}

class _DrawerTile extends StatelessWidget {
  final Widget? leading;
  final Widget title;

  const _DrawerTile({this.leading, required this.title});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: leading ?? const SizedBox(width: 28, height: 28),
      title: title,
      onTap: () => context.go('$title'),
    );
  }
}

// class _Divider extends StatelessWidget {
//   const _Divider({required this.text});
//   final String text;
//   @override
//   Widget build(BuildContext context) {
//     return SizedBox(
//         width: MediaQuery.of(context).size.width * 1,
//         child: Container(
//           decoration: const BoxDecoration(
//             color: Color(0xff004881),
//           ),
//           child: Padding(
//             padding: const EdgeInsets.all(8.0),
//             child: Text(
//               text,
//               style: const TextStyle(
//                 color: Colors.white,
//               ),
//             ),
//           ),
//         ));
//   }
// }

class _Page extends StatelessWidget {
  const _Page({required this.name, required this.url});
  final String name;
  final String url;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => context.go(url),
      child: Text(
        name,
        style: Theme.of(context).textTheme.bodyLarge,
      ),
    );
  }
}
